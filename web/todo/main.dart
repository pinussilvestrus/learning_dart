// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'package:vector_math/vector_math.dart';

InputElement todoInput;
UListElement todoList;
LinkElement deleteAll;

void main() {
  todoInput = querySelector('#to-do-input');
  todoList = querySelector('#to-do-list');
  deleteAll = querySelector('#delete-all');

  todoInput.onChange.listen(addToDoItem);
  deleteAll.onClick.listen((e) { todoList.children.clear(); deleteAll.hidden = true;});
}

void addToDoItem(Event e) {

  var newItem = new LIElement();
  newItem.text = todoInput.value;
  newItem.classes.add('collection-item');

  AnchorElement iconA = new AnchorElement();
  iconA.classes.add("secondary-content");
  LIElement icon = new LIElement();
  icon.classes.add("fa");
  icon.classes.add("fa-check-circle");
  iconA.children.add(icon);
  iconA.onClick.listen((e) {
    newItem.remove();
    if (todoList.children.length == 0) {
      deleteAll.hidden = true;
    }
  });
  newItem.children.add(iconA);

  todoInput.value = '';
  todoList.children.add(newItem);
  deleteAll.hidden = false;
}

void removeElement(LIElement item) {
  item.remove();
  if (todoList.children.length == 0) {
    deleteAll.hidden = true;
  }
}
